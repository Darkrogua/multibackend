<?php namespace ArtDark\Multibackend;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = ['Keios.Multisite'];

    public function registerReportWidgets()
    {
        return [
            'ArtDark\Multibackend\Widgets\SwitcherTheme' => [
                'label' => 'artdark.multibackend::lang.widget.widget_name',
            ],
        ];
    }
}
