<?php return [
    'plugin' => [
        'name' => 'multibackend',
        'description' => ''
    ],
    'widget' => [
        'widget_name' => 'Переключатель тем',
        'success_update' => 'Успешно переключили тему!',
        'update_btn_text' => 'Переключить',
    ],
];
