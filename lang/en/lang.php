<?php return [
    'plugin' => [
        'name' => 'multibackend',
        'description' => 'Simple plugin switch theme your backend'
    ],
    'widget' => [
        'widget_name' => 'Theme switcher',
        'success_update' => 'Successfully switched theme!',
        'update_btn_text' => 'Switch',
    ],
];
