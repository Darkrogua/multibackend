<?php namespace ArtDark\Multibackend\Widgets;

use Backend\Classes\ReportWidgetBase;
use Flash;
use Lang;
use Twig;
use Backend\Facades\BackendAuth;
use DB;
use Input;
use Keios\Multisite\Models\Setting;

class SwitcherTheme extends ReportWidgetBase
{
    /**
     * Render method
     * @return mixed|string
     * @throws \SystemException
     */
    protected $themes;
    protected $user_id;

    public function  __construct($controller, $properties = [])
    {
        parent::__construct($controller, $properties);
        $this->themes = Setting::lists('theme');
        $user = BackendAuth::getUser();
        $this->user_id = $user->id;
    }

    public function render()
    {
        $activeThemeConfig = DB::table('backend_user_preferences')
            ->where([['group','theme'],['user_id', $this->user_id]])->pluck('value')->first();
        $activeThemeConfig = str_replace('"', '', $activeThemeConfig);
        $content = $this->makePartial('widget');
        $twig_options = ['themes' => $this->themes, 'active' => $activeThemeConfig];
        return $this->makeTwigPartial($content, $twig_options);
    }

    public function makeTwigPartial($content, $options = null)
    {
        return Twig::parse($content, $options);
    }


    public function onSwitchTheme() {
        $activeTheme = Input::get('theme');

        DB::table('backend_user_preferences')
            ->where([['item','edit'],['group','theme'],['user_id', $this->user_id]])->update(['value' => '"'.$activeTheme.'"']);
        Flash::success(Lang::get('artdark.multibackend::lang.widget.success_update'));
    }


}
